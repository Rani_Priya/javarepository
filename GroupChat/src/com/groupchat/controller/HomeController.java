package com.groupchat.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.kumaresh.dao.CommentDao;

@Controller
public class HomeController {
	
	@Autowired
	CommentDao commentDao;
	
	@RequestMapping("/")
	public String showHome(){
		return "login";
	}
	
	@RequestMapping(value="/register")
	public String showRegister(){
		return "register";
	}
	
	@RequestMapping(value="/login")
	public String showLogin(){
		return "login";
	}
	
	@RequestMapping(value="/refreshAction")
	public ModelAndView fetchComments(){
		
		List<String> listComment = commentDao.fetchComments();
		ModelAndView model = new ModelAndView();
		model.addObject("userComment", listComment);
		model.setViewName("success");
		
		return model;
	}
}
