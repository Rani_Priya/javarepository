package com.groupchat.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.groupchat.login.dao.LoginUser;
import com.groupchat.model.LoginUserModel;
import com.kumaresh.dao.CommentDao;
import com.kumaresh.model.Comment;

@Controller
@SessionAttributes("emailid")
public class LoginController {
	
	@Autowired
	private LoginUser loginUser;
	
	@Autowired
	private CommentDao commentDao;

	@RequestMapping(value="/LoginAction", method=RequestMethod.POST)
	public String fetchLoginCredentials(@ModelAttribute("loginModel") LoginUserModel loginUserModel,
			ModelMap model){
	
		boolean checkLogin = loginUser.checkLogin(loginUserModel);
		
		if(checkLogin == true){
			model.addAttribute("emailid", loginUserModel.getEmailId());
			return "success";
		}
		
		else {
			return "login";
		}
		
	}
	
	@RequestMapping(value="/commentAction", method=RequestMethod.POST)
	public String processComment(@ModelAttribute("commentModel") Comment comment, ModelMap model){
		
		String mailid = model.get("emailid").toString();
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		String dateStr = dateFormat.format(date);
		
		if(comment.getUserComment().length() > 0){
			String insertComment = dateStr+" : "+mailid+" : "+comment.getUserComment();
			
			commentDao.insertComment(insertComment);
			
			List<String> commentMsg = commentDao.fetchComments();
			
			model.addAttribute("userComment", commentMsg);
			
			return "success";
		}
		else {
			List<String> commentMsg = commentDao.fetchComments();
			
			model.addAttribute("userComment", commentMsg);
			
			return "success";
		}
		
	}
	
	
}
