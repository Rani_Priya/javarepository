package com.kumaresh.dao;

import java.util.List;

public interface CommentDao {

	public void insertComment(String insertComment);
	
	public List<String> fetchComments();
	
}
